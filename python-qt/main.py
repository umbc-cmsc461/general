from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

from PyQt5 import QtCore, QtGui, uic

import uuid
import sys

'''
The MainWindow class.  Loads our Qt UI file and makes the necessary connections.
'''
class MainWindow(QMainWindow):
    '''
    MainWindow's constructor.
    '''
    def __init__(self):
# Call to the parent constructor -- we MUST do this
        super(MainWindow, self).__init__()

# Load the UI file and shove it into this class
# You don't have to do this and you can program the UI yourself from code,
# but this is much easier, IMO.
        self.ui = uic.loadUi("MainWindow.ui", self)
        self.ui.show()

# Connect signals and slots -- clicked connects to a function
        self.ui.randomUuidButton.clicked.connect(self.displayUuid)
        self.ui.displayDialogButton.clicked.connect(self.displayDialog)
        self.ui.exitButton.clicked.connect(self.exitProgram)

# This is for the menu bar
        self.ui.actionExit.triggered.connect(self.exitMenu)

    '''
    This function will display a random UUID in the display label.
    '''
    def displayUuid(self):
        self.ui.textDisplayLabel.setText(str(uuid.uuid4()))

    '''
    This function will display a sample dialog and display the choice
    in the display label.
    '''
    def displayDialog(self):
        i = InfoDialog(self, "Here's a dialog!")
        i.show()

        i.ui.buttonBox.accepted.connect(self.dialogAccept)
        i.ui.buttonBox.rejected.connect(self.dialogReject)

    '''
    This function is connected to an InfoDialog's accepted SIGNAL.
    '''
    def dialogAccept(self):
        self.ui.textDisplayLabel.setText("Dialog accepted!")

    '''
    This function is connected to an InfoDialog's rejected SIGNAL.
    '''
    def dialogReject(self):
        self.ui.textDisplayLabel.setText("Dialog rejected!")


    '''
    This exits the program.  Prompts the user first before exiting.
    This SLOT is used by the exit button on the main window.
    '''
    def exitProgram(self):
        i = InfoDialog(self, "From the button: Are you sure you want to quit?")
        i.show()

        i.ui.buttonBox.accepted.connect(self.reallyQuit)

    '''
    This exits the program.  Prompts the user first before exiting.
    This SLOT is used by the exit menu item.
    '''
    def exitMenu(self, action):
        i = InfoDialog(self, "From the menu: Are you sure you want to quit?")
        i.show()

        i.ui.buttonBox.accepted.connect(self.reallyQuit)

    '''
    This really exits the program.
    '''
    def reallyQuit(self):
        QApplication.quit()

'''
The InfoDialog class.  Loads our Qt UI file and makes the necessary connections.
'''
class InfoDialog(QDialog):
    def __init__(self, parent, message):
        super(InfoDialog, self).__init__(parent)

        self.ui = uic.loadUi("InfoDialog.ui", self)
        self.ui.show()

        self.ui.infoDisplay.setText(message)
# Doesn't do much other than this -- up to other classes to connect SLOTS to it

'''
Main function.
'''
if __name__ == '__main__':
# Pass arguments to the QApplication object.  Necessary for all Qt programs.
    app = QApplication(sys.argv)

# Instantiate the Main Window and show it.
    mw = MainWindow()
    mw.show()

# Exit the program once the QApplication is done executing.
    sys.exit(app.exec_())
