# Example Maven project

In this folder is an example of a Maven project entirely checked into git.  Some things to go over:

## Code in git

This code is entirely checked into git.  When using git or any other source control system, you should set your project up within your source repository and code entirely within the confines of the repository.  You should not copy files into git periodically and copy them out to work on them.

Let me reiterate.  **You should never write a single line of code unless the file is inside of your git repository.**  Git is capable of handling eclipse files, ant builds, maven poms, etc.  Your **entire** project should sit inside of your git repository at all times.

## .gitignore

There are certain files that you generally don't want tracked by your git repository.  Many of these include files that you compile (the resulting jar or .o files from your builds).  There's an example .gitignore file in this repository.  Please examine it.  It contains a list of specific files and wildcards that describes what kind of files should be ignored by git.

## Maven

This project was generated initially by running the following command:

```
mvn archetype:generate -DgroupId=cmsc.db -DartifactId=maven-project -DarchitypeArtifictId=maven-archetype-quickstart -DinteractiveMode=false
```

This will set up a skeleton framework for you to work out of.  **Maven must be installed to run this command.**  Various IDEs have ways of accomplishing the same functionality -- you will need to look up exactly how to do that.  Regardless, you should set your project up **inside of your git repository**, and open the project with whatever IDE you are using.  Some IDEs require you to "import" maven projects, some open them directly.  You'll have to research what your editor will do.

## pom.xml

I've modified the pom.xml to do 3 interesting things:

* Copy the dependencies into dist/libs.  This makes it easy to run from the command line, as all of our dependencies are in an easily located place.
* Copy the compiled jar into dist.  As above, this makes it easy to run from the command line.
* Clean the dist directory in addition to other things maven usually cleans.

To run your program in the dist directory, you would need to navigate to the dist directory and run:

```
java -cp <compiled-jar>.jar:lib/* <main-class>
```

## JUnit

I've implemented a set of JUnit tests here.  These tests exercise the code written thoroughly.  It is not 100%, but it's close.  JUnit allows us to write a lot of customized code and design a set of conditions that must be met for the tests to be considered "successful."  It's a lot easier to test your code when you use a framework like JUnit.

## Hibernate

This project also utilizes Hibernate, but a little differently from the way I described it in class.  Instead of using an XML configuration file, I use hibernate.properties.  Additionally, instead of setting up mapping files, I instead use annotations to describe the model classes *in the code itself*.  This makes things a little bit more concise in my opinion.

## Questions

Please e-mail me with any questions, and I will add them here with their answers.
