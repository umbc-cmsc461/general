package cmsc.db;

import cmsc.db.mapping.Assignment;
import cmsc.db.mapping.GradeBookEntry;
import cmsc.db.mapping.Student;
import org.apache.log4j.Logger;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

/**
 * Unit tests for the DBInterface.
 */
public class DBInterfaceTest
{
    private static Logger logger = Logger.getLogger(DBInterfaceTest.class);

    /**
     * Student related tests.
     */
    @Test
    public void studentTest()
    {
        try {
            DBInterface dbInterface = DBInterface.getInstance();

            // Add a few students
            Date curDate = new Date();
            int id1 = dbInterface.addStudent("a", "b", "c", curDate);
            int id2 = dbInterface.addStudent("d", "e", "f", curDate);
            int id3 = dbInterface.addStudent("g", "h", "i", curDate);
            int id4 = dbInterface.addStudent("j", "k", "l", curDate);

            // Test the single get
            Student student2 = dbInterface.getStudent(id2);
            assertEquals(student2.getMiddleName(), "e");
            assertEquals(student2.getLastName(), "f");

            // Test the bulk retrieval
            List<Student> students = dbInterface.getStudents(Arrays.asList(id1, id2, id4));
            Map<Integer, Student> studentMap = new HashMap<Integer, Student>();

            // Store off the students into a map
            for(Student student : students) {
                studentMap.put(student.getId(), student);
            }

            // Make sure the returned results maps up with our inserted entries
            assertEquals(studentMap.get(id1).getFirstName(), "a");
            assertEquals(studentMap.get(id1).getMiddleName(), "b");
            assertEquals(studentMap.get(id1).getLastName(), "c");
            assertEquals(studentMap.get(id1).getDateOfBirth(), curDate);

            assertEquals(studentMap.get(id2).getFirstName(), "d");
            assertEquals(studentMap.get(id2).getMiddleName(), "e");
            assertEquals(studentMap.get(id2).getLastName(), "f");
            assertEquals(studentMap.get(id2).getDateOfBirth(), curDate);

            assert(studentMap.keySet().contains(id3) == false);

            assertEquals(studentMap.get(id4).getFirstName(), "j");
            assertEquals(studentMap.get(id4).getMiddleName(), "k");
            assertEquals(studentMap.get(id4).getLastName(), "l");
            assertEquals(studentMap.get(id4).getDateOfBirth(), curDate);

            // Make sure the removal works
            dbInterface.removeStudent(id2);

            List<Student> students2 = dbInterface.getStudents(Arrays.asList(id1, id2, id4));

            boolean foundId2 = false;
            for(Student student : students2) {
                if(student.getId() == id2) {
                    foundId2 = true;
                }
            }

            assertFalse(foundId2);
        }
        catch(Exception e) {
            logger.error("Error during studentTest.", e);
            fail();
        }
    }

    /**
     * Assignment related tests.
     */
    @Test
    public void assignmentTest()
    {
        try {
            DBInterface dbInterface = DBInterface.getInstance();

            // Add a few assignments
            int id1 = dbInterface.addAssignment("a", "b", 1);
            int id2 = dbInterface.addAssignment("c", "d", 2);
            int id3 = dbInterface.addAssignment("e", "f", 3);
            int id4 = dbInterface.addAssignment("g", "h", 4);

            // Test the single get
            Assignment student2 = dbInterface.getAssignment(id2);
            assertEquals(student2.getName(), "c");
            assertEquals(student2.getDescription(), "d");
            assert(student2.getWeight() == 2);

            // Test the bulk retrieval
            List<Assignment> assignments = dbInterface.getAssignments(Arrays.asList(id1, id2, id4));
            Map<Integer, Assignment> assignmentMap = new HashMap<Integer, Assignment>();

            // Store off the students into a map
            for(Assignment assignment : assignments) {
                assignmentMap.put(assignment.getId(), assignment);
            }

            // Make sure the returned results maps up with our inserted entries
            assertEquals(assignmentMap.get(id1).getName(), "a");
            assertEquals(assignmentMap.get(id1).getDescription(), "b");
            assert(assignmentMap.get(id1).getWeight() == 1);

            assertEquals(assignmentMap.get(id2).getName(), "c");
            assertEquals(assignmentMap.get(id2).getDescription(), "d");
            assert(assignmentMap.get(id2).getWeight() == 2);

            assert(assignmentMap.keySet().contains(id3) == false);

            assertEquals(assignmentMap.get(id4).getName(), "g");
            assertEquals(assignmentMap.get(id4).getDescription(), "h");
            assert(assignmentMap.get(id4).getWeight() == 4);

            // Make sure the removal works
            dbInterface.removeAssignment(id2);

            List<Assignment> assignments2 = dbInterface.getAssignments(Arrays.asList(id1, id2, id4));

            boolean foundId2 = false;
            for(Assignment assignment : assignments2) {
                if(assignment.getId() == id2) {
                    foundId2 = true;
                }
            }

            assertFalse(foundId2);
        }
        catch(Exception e) {
            logger.error("Error during studentTest.", e);
            fail();
        }
    }

    /**
     * GradeBookEntry related tests
     */
    @Test
    public void gradeBookEntryTest()
    {
        try {
            DBInterface dbInterface = DBInterface.getInstance();

            // Add a few assignments
            Date curDate = new Date();
            int sid1 = dbInterface.addStudent("a", "b", "c", curDate);
            int sid2 = dbInterface.addStudent("d", "e", "f", curDate);
            int sid3 = dbInterface.addStudent("g", "h", "i", curDate);
            int sid4 = dbInterface.addStudent("j", "k", "l", curDate);

            // Add a few assignments
            int aid1 = dbInterface.addAssignment("a", "b", 1);
            int aid2 = dbInterface.addAssignment("c", "d", 2);
            int aid3 = dbInterface.addAssignment("e", "f", 3);
            int aid4 = dbInterface.addAssignment("g", "h", 4);

            // Finally, add a few gradebook entries
            int id1 = dbInterface.addGradeBookEntry(sid1, aid1, 100);
            int id2 = dbInterface.addGradeBookEntry(sid2, aid2, 200);
            int id3 = dbInterface.addGradeBookEntry(sid3, aid3, 300);
            int id4 = dbInterface.addGradeBookEntry(sid4, aid4, 400);

            // Test the single get
            GradeBookEntry gradeBookEntry2 = dbInterface.getGradeBookEntry(id2);
            assertEquals(gradeBookEntry2.getAssignment(), dbInterface.getAssignment(aid2));
            assertEquals(gradeBookEntry2.getStudent(), dbInterface.getStudent(sid2));
            assert(gradeBookEntry2.getGrade() == 200);

            // Test the bulk retrieval
            List<GradeBookEntry> gradeBookEntries = dbInterface.getGradeBookEntries(Arrays.asList(id1, id2, id4));
            Map<Integer, GradeBookEntry> gradeBookEntryMap = new HashMap<Integer, GradeBookEntry>();

            // Store off the list of IDs retrieved
            for(GradeBookEntry gradeBookEntry : gradeBookEntries) {
                gradeBookEntryMap.put(gradeBookEntry.getId(), gradeBookEntry);
            }

            // Make sure the returned results maps up with our inserted entries
            assertEquals(gradeBookEntryMap.get(id1).getStudent(), dbInterface.getStudent(sid1));
            assertEquals(gradeBookEntryMap.get(id1).getAssignment(), dbInterface.getAssignment(aid1));
            assert(gradeBookEntryMap.get(id1).getGrade() == 100);

            assertEquals(gradeBookEntryMap.get(id2).getStudent(), dbInterface.getStudent(sid2));
            assertEquals(gradeBookEntryMap.get(id2).getAssignment(), dbInterface.getAssignment(aid2));
            assert(gradeBookEntryMap.get(id2).getGrade() == 200);

            assert(gradeBookEntryMap.keySet().contains(id3) == false);

            assertEquals(gradeBookEntryMap.get(id4).getStudent(), dbInterface.getStudent(sid4));
            assertEquals(gradeBookEntryMap.get(id4).getAssignment(), dbInterface.getAssignment(aid4));
            assert(gradeBookEntryMap.get(id4).getGrade() == 400);

            // Make sure the removal works
            dbInterface.removeGradeBookEntry(id2);

            List<GradeBookEntry> gradeBookEntries2 = dbInterface.getGradeBookEntries(Arrays.asList(id1, id2, id4));

            boolean foundId2 = false;
            for(GradeBookEntry gradeBookEntry : gradeBookEntries2) {
                if(gradeBookEntry.getId() == id2) {
                    foundId2 = true;
                }
            }

            assertFalse(foundId2);
        }
        catch(Exception e) {
            logger.error("Error during studentTest.", e);
            fail();
        }
    }
}
