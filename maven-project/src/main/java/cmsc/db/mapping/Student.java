package cmsc.db.mapping;

import javax.persistence.*;
import java.util.Date;

/**
 * An annotated hibernate class that represents students.
 */
@Entity
@Table(name = "students")
public class Student {
    @Id @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "middle_name")
    private String middleName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "dob")
    private Date dateOfBirth;

    /**
     * Gets the ID for the current student.
     * @return The student's ID.
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the ID for the current student.
     * @param id The ID to give to the student.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Returns the student's first name.
     * @return The student's first name.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the student's first name.
     * @param firstName The first name to give to the student.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Returns the student's middle name.
     * @return The student's middle name.
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Sets the student's middle name.
     * @param middleName The middle name to give to the student.
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * Returns the student's last name.
     * @return The student's last name.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the student's last name.
     * @param lastName The last name to give to the student.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Returns the birth date of the student.
     * @return The birth date of the student.
     */
    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Sets the birth date of the student.
     * @param dateOfBirth The birth date to give to the student.
     */
    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
}
