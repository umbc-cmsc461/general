package cmsc.db.mapping;


import javax.persistence.*;

/**
 * An annotated hibernate class that represents our address book.
 */
@Entity
@Table(name = "GradeBook")
public class GradeBookEntry {
    @Id @GeneratedValue
    @Column(name = "id")
    private int id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "student_id")
    private Student student;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "assignment_id")
    private Assignment assignment;

    @Column(name = "grade")
    private double grade;

    /**
     * Returns the ID for the grade book entry.
     * @return A gradebook entry ID.
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the ID for the grade book entry.
     * @param id The ID to set the current gradebook entry to.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Returns the student object currently represented in the gradebook entry.
     * @return A student object.
     */
    public Student getStudent() {
        return student;
    }

    /**
     * Sets the student object for the gradebook entry.
     * @param student The student object to set the current gradebook entry to.
     */
    public void setStudent(Student student) {
        this.student = student;
    }

    /**
     * Returns the assignment object currentlty represented in the gradebook entry.
     * @return An assignment object.
     */
    public Assignment getAssignment() {
        return assignment;
    }

    /**
     * Sets the assignment object for the gradebook entry.
     * @param assignment The assignment object to set the current gradebook entry to.
     */
    public void setAssignment(Assignment assignment) {
        this.assignment = assignment;
    }

    /**
     * Returns the student's grade for a particular assignment.
     * @return The student's grade for the assignment.
     */
    public double getGrade() {
        return grade;
    }

    /**
     * Sets the student's grade for the assignment.
     * @param grade The grade to give for the assignment.
     */
    public void setGrade(double grade) {
        this.grade = grade;
    }
}
