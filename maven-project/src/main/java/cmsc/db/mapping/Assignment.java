package cmsc.db.mapping;

import javax.persistence.*;

/**
 * An annotated hibernate class representing assigments.
 */
@Entity
@Table(name = "assignments")
public class Assignment {
    @Id @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "weight")
    private double weight;

    /**
     * Gets the ID for the current assignment.
     * @return The assignment ID.
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the ID for the current assignment.
     * @param id The ID to give to the assignment.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Returns the assignment's name.
     * @return The assignment's name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name for the current assignment.
     * @param name The name to give to the assignment.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns a description of the current assignment.
     * @return The assignment description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description for the current assignment.
     * @param description The description to give to the assignment.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the weight of the assignment's grade.
     * @return The weight of the assignment.
     */
    public double getWeight() {
        return weight;
    }

    /**
     * Sets the weight of the assignment's grade.
     * @param weight The weight to give to the assignment.
     */
    public void setWeight(double weight) {
        this.weight = weight;
    }
}
