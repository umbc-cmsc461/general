package cmsc.db;

import cmsc.db.mapping.Assignment;
import cmsc.db.mapping.GradeBookEntry;
import cmsc.db.mapping.Student;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistryBuilder;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * This is our database interface class.  It abstracts a lot of the pain away from the user of the DBInterface.
 */
public class DBInterface {
    // The logger for the class
    private static Logger logger = Logger.getLogger(DBInterface.class);

    // Singleton instance of the DBInterface
    private static DBInterface instance = null;

    // Hibernate variables used for the interface
    private SessionFactory sFactory;
    private Session session;

    // This is a singleton object, so we make the constructor private
    private DBInterface() {
        // Configure Hibernate to recognize our classes
        Configuration configuration = new Configuration();

        // In addition to the mapping files we discussed in class, we can also "annotate" our classes
        // in order to describe them to Hibernate.  That's what I've done here.  In order to make sure
        // Hibernate knows about them, I add them like this.
        configuration.addAnnotatedClass(Student.class);
        configuration.addAnnotatedClass(Assignment.class);
        configuration.addAnnotatedClass(GradeBookEntry.class);

        // Create the session factory and session
        sFactory = configuration.buildSessionFactory(new ServiceRegistryBuilder().buildServiceRegistry());
        session = sFactory.openSession();

        // Add a shutdown hook with our cleanup thread
        // This will ensure that the DBInterface cleans up after itself when the JVM shuts down
        Runtime.getRuntime().addShutdownHook(new CleanupThread());
    }

    /**
     * This is the static method that returns an instance of the DBInterface.
     * @return The current instance of the DBInterface.
     */
    public synchronized static DBInterface getInstance() {
        if(instance == null) {
            instance = new DBInterface();
        }

        return instance;
    }

    /**
     * Gets a student from the database given an identifier.
     * @param studentId The identifier of the student to obtain.
     * @return
     */
    public Student getStudent(int studentId) {
        return (Student) session.get(Student.class, studentId);
    }

    /**
     * Gets a list of students given a list of student IDs.
     * @param studentIds The list of student IDs to obtain.
     * @return A list of students.
     */
    public List<Student> getStudents(List<Integer> studentIds) {
        Query query = session.createQuery("from Student where id in :students");
        query.setParameterList("students", studentIds);

        return query.list();
    }

    /**
     * Adds a student to the database.
     * @param firstName The first name of the new student.
     * @param middleName The middle name of the new student.
     * @param lastName The last name of the new student.
     * @param dateOfBirth The date of birth of the new student.
     * @return Returns the identifier for the recently created student.
     */
    public int addStudent(String firstName, String middleName, String lastName, Date dateOfBirth) {
        Student student = new Student();

        // Assign relevant variables to the student object
        student.setFirstName(firstName);
        student.setMiddleName(middleName);
        student.setLastName(lastName);
        student.setDateOfBirth(dateOfBirth);

        // Save it to our database
        session.beginTransaction();
        session.save(student);
        session.getTransaction().commit();

        // Return the newly generate ID
        return student.getId();
    }

    /**
     * Removes a student from the database.
     * @param studentId The student ID to remove.
     */
    public void removeStudent(int studentId) {
        session.beginTransaction();
        session.delete(session.load(Student.class, studentId));
        session.getTransaction().commit();
    }

    /**
     * Gets an assignment from the database given an identifier.
     * @param assignmentId The identifier of the assignment to obtain.
     * @return
     */
    public Assignment getAssignment(int assignmentId) {
        return (Assignment) session.get(Assignment.class, assignmentId);
    }

    /**
     * Gets a list of assignments given a list of assignment IDs.
     * @param assignmentIds The list of assignment IDs to obtain.
     * @return A list of assignments.
     */
    public List<Assignment> getAssignments(List<Integer> assignmentIds) {
        Query query = session.createQuery("from Assignment where id in :assignments");
        query.setParameterList("assignments", assignmentIds);

        return query.list();
    }

    /**
     * Adds an assignment to the database.
     * @param assignmentName An assignment name for the assignment.
     * @param description A description of the assignment.
     * @param weight The weight of the assignment.
     * @return Returns the identifier for the recently created assignment.
     */
    public int addAssignment(String assignmentName, String description, double weight) {
        Assignment assignment = new Assignment();

        // Assign relevant variables to the assignment object
        assignment.setName(assignmentName);
        assignment.setDescription(description);
        assignment.setWeight(weight);

        // Save it to our database
        session.beginTransaction();
        session.save(assignment);
        session.getTransaction().commit();

        // Return the newly generate ID
        return assignment.getId();
    }

    /**
     * Removes an assignment from the database.
     * @param assignmentId The assignment ID to remove.
     */
    public void removeAssignment(int assignmentId) {
        session.beginTransaction();
        session.delete(session.load(Assignment.class, assignmentId));
        session.getTransaction().commit();
    }

    /**
     * Gets an grade book entry from the database given an identifier.
     * @param gradeBookEntryId The identifier of the grade book entry to obtain.
     * @return
     */
    public GradeBookEntry getGradeBookEntry(int gradeBookEntryId) {
        return (GradeBookEntry) session.get(GradeBookEntry.class, gradeBookEntryId);
    }

    /**
     * Gets a list of grade book entries given a list of grade book entry IDs.
     * @param gradeBookEntryIds The list of grade book entry IDs to obtain.
     * @return A list of grade book entries.
     */
    public List<GradeBookEntry> getGradeBookEntries(List<Integer> gradeBookEntryIds) {
        Query query = session.createQuery("from GradeBookEntry where id in :gradebookentries");
        query.setParameterList("gradebookentries", gradeBookEntryIds);

        return query.list();
    }

    /**
     * Adds a new graded assignment for a student into our gradebook.
     * @param studentId The ID of the student to add the grade for.
     * @param assignmentId The ID of the assignment to add the grade for.
     * @param grade The grade to add.
     * @return The ID of the GradeBookEntry
     */
    public int addGradeBookEntry(int studentId, int assignmentId, double grade) {
        GradeBookEntry gradeBookEntry = new GradeBookEntry();

        // Assign relevant variables to the gradebook entry
        // Use session.load for setstudent and setassignment -- this lets us avoid hitting the database each object we
        // need to join to the gradebookentry
        gradeBookEntry.setStudent((Student) session.load(Student.class, studentId));
        gradeBookEntry.setAssignment((Assignment) session.load(Assignment.class, assignmentId));
        gradeBookEntry.setGrade(grade);

        // Save it to our database
        session.beginTransaction();
        session.save(gradeBookEntry);
        session.getTransaction().commit();

        return gradeBookEntry.getId();
    }

    /**
     * Removes a gradebookentry from the database.
     * @param gradeBookEntryId The grade book entry ID to remove.
     */
    public void removeGradeBookEntry(int gradeBookEntryId) {
        session.beginTransaction();
        session.delete(session.load(GradeBookEntry.class, gradeBookEntryId));
        session.getTransaction().commit();
    }

    private class CleanupThread extends Thread {
        @Override
        public void run() {
            // Close the session if we need to
            if(session != null) {
                logger.info("Cleaning up the session.");
                session.close();
            }

            // Close the session factory if we need to
            if(sFactory != null) {
                logger.info("Cleaning up the session factory.");
                sFactory.close();
            }

            logger.info("Done cleaning up DB variables!");
        }
    }
}
