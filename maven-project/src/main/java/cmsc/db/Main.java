package cmsc.db;

import cmsc.db.mapping.Assignment;
import cmsc.db.mapping.GradeBookEntry;
import cmsc.db.mapping.Student;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

/**
 * Sample main driver.
 */
public class Main {
    private static Logger logger = Logger.getLogger(Main.class);

    public static void main(String [] args) {
        try {
            // Get the database interface
            DBInterface dbInterface = DBInterface.getInstance();
            SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd");

            // Insert an assignment
            int aId =
                    dbInterface.addAssignment("Example project",
                                              "An example project that is worth 100% of the grade!",
                                              1);

            // Inserts students
            int student1 = dbInterface.addStudent("Henry", "J", "Henryman", sdFormat.parse("1990-01-01"));
            int student2 = dbInterface.addStudent("Heather", "B", "Kendall", sdFormat.parse("1989-06-23"));
            int student3 = dbInterface.addStudent("Mike", "S", "Duncan", sdFormat.parse("1990-08-12"));

            // Insert grade book entries
            int gbid1 = dbInterface.addGradeBookEntry(student1, aId, 98);
            int gbid2 = dbInterface.addGradeBookEntry(student2, aId, 100);
            int gbid3 = dbInterface.addGradeBookEntry(student3, aId, 95);

            System.out.println("-------------------------------------------------------------------");
            // Get a bunch of grade book entries
            List<GradeBookEntry> gradeBookEntryList = dbInterface.getGradeBookEntries(Arrays.asList(gbid1, gbid2, gbid3));

            for(GradeBookEntry gradeBookEntry : gradeBookEntryList) {
                Student student = gradeBookEntry.getStudent();
                Assignment assignment = gradeBookEntry.getAssignment();

                System.out.println("Student ID: " + student.getId() +
                        ", Name: " + student.getFirstName() + " " + student.getMiddleName() + " " +
                        student.getLastName() + ", DOB: " + sdFormat.format(student.getDateOfBirth()));
                System.out.println("Assignment ID: " + assignment.getId() +
                        ", Name: " + assignment.getName() + ", Description: " + assignment.getDescription() + ", " +
                        "Weight: " + assignment.getWeight());
                System.out.println("Grade ID: " + gradeBookEntry.getId() + ", Grade: " + gradeBookEntry.getGrade());
                System.out.println("-------------------------------------------------------------------");
            }
        }
        catch(Exception e) {
            logger.error("Error during main execution!", e);
        }
        finally {

            logger.info("Done with cleanup!");
        }
    }
}
